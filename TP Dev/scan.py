from pickle import NONE
import socket
from scapy.all import *
from scapy.layers.inet import IP, ICMP


for ping in range(1, 255):
    address = f"192.168.1.{ping}"
    socket.setdefaulttimeout(1)

    try:
        hostname, alias, addresslist = socket.gethostbyaddr(address)

    except socket.herror:
        hostname = None
        alias = None
        addresslist = address

    os = ''
    target = address
    pack = IP(dst=target)/ICMP()
    resp = sr1(pack, timeout=3)
    if resp:
        if IP in resp:
            ttl = resp.getlayer(IP).ttl
            if ttl <= 64:
                os = 'Linux'
            elif ttl > 64:
                os = 'Windows'
            else:
                os = 'MacOS'

    if os != '':
        if hostname != '':
            print(addresslist, '=>q', hostname, os)
        else:
            continue
    else:
        continue

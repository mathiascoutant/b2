I. Exploration locale en solo

1. Affichage d'informations sur la pile TCP/IP locale

Nom et interface wifi : 

```markdown
mathias@MacBook-Air-de-Mathias ~ % ifconfig

en0: flags=8863<UP,BROADCAST,SMART,RUNNING,SIMPLEX,MULTICAST> mtu 1500
options=6463<RXCSUM,TXCSUM,TSO4,TSO6,CHANNEL_IO,PARTIAL_CSUM,ZEROINVERT_CSUM>
ether 18:3e:ef:d7:78:db 
inet6 fe80::c39:2527:e835:b9bb%en0 prefixlen 64 secured scopeid 0xb 
inet 10.33.19.152 netmask 0xfffffc00 broadcast 10.33.19.255
nd6 options=201<PERFORMNUD,DAD>
media: autoselect
status: active
```




Afficher Gateway :

```markdown
mathias@MacBook-Air-de-Mathias ~ % route get default |grep gateway
gateway: 10.33.19.254
```




Graphiquement afficher les informations sur une carte IP :


![toto](./01.png)




2. Modifications des informations

A. Modification d'adresse IP

![toto](./02.png)

![toto](./03.png)





III. Manipulations d'autres outils/protocoles côté client

1. DHCP

afficher l'adresse IP du serveur DHCP du réseau WiFi YNOV :

10.33.19.152 





date d'expiration de votre bail DHCP :

```markdown 
mathias@MacBook-Air-de-Mathias ~ % sudo cat /var/db/dhcpclient/leases/en0.plist

<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
	<key>ClientIdentifier</key>
	<data>
	ARg+79d42w==
	</data>
	<key>IPAddress</key>
	<string>10.33.19.152</string>
	<key>LeaseLength</key>
	<integer>73047</integer>
	<key>LeaseStartDate</key>
	<date>2022-09-27T15:01:09Z</date>
	<key>PacketData</key>
	<data>
	AgEGAJF9ghsAAAAAAAAAAAohE5gAAAAAAAAAABg+79d42wAAAAAAAAAAAAAAAAAAAAAA
	AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
	AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
	AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
	AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAABjglNjNQEFNgQKIRP+MwQAAR1X
	AQT///wAAwQKIRP+BgwICAgICAgEBAEBAQH/AAAAAAAAAAAAAAAAAAAAAAAA
	</data>
	<key>RouterHardwareAddress</key>
	<data>
	AMDn4ARO
	</data>
	<key>RouterIPAddress</key>
	<string>10.33.19.254</string>
	<key>SSID</key>
	<string>WiFi@YNOV</string>
</dict>
</plist>
```


# II. Exploration locale en duo


🌞Si vos PCs ont un port RJ45 alors y'a une carte réseau Ethernet associée :

PS C:\Users\heroeselfric> ipconfig /all

Carte Ethernet Ethernet :
[...]
   Adresse IPv4. . . . . . . . . . . . . .: 192.168.18.2(préféré)
   Masque de sous-réseau. . . . . . . . . : 255.255.255.252
[...]
pour verifier la connection avec lautre ordinateur 

PS C:\Users\heroeselfric> ping 192.168.18.1

Envoi d’une requête 'Ping'  192.168.18.1 avec 32 octets de données :
Réponse de 192.168.18.1 : octets=32 temps=1 ms TTL=128
Réponse de 192.168.18.1 : octets=32 temps=1 ms TTL=128
Réponse de 192.168.18.1 : octets=32 temps=2 ms TTL=128
Réponse de 192.168.18.1 : octets=32 temps=1 ms TTL=128

Statistiques Ping pour 192.168.18.1:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 1ms, Maximum = 2ms, Moyenne = 1ms
- 🌞 pour tester la connectivité à internet on fait souvent des requêtes simples vers un serveur internet connu
  - encore une fois, un ping vers un DNS connu comme `1.1.1.1` ou `8.8.8.8` c'est parfait
ca c le debut je tenverrais la suite plus tard
change les infos stp



2. DNS

l'adresse IP du serveur DNS:

```markdown 
mathias@MacBook-Air-de-Mathias ~ % ipconfig getifaddr en0
10.33.19.152
```



lookup google.com :

```markdown 
mathias@MacBook-Air-de-Mathias ~ % nslookup google.com 
Server:		8.8.8.8
Address:	8.8.8.8#53

Non-authoritative answer:
Name:	google.com
Address: 216.58.215.46
```



lookup ynov.com:

```markdown 
mathias@MacBook-Air-de-Mathias ~ % nslookup ynov.com  
Server:		8.8.8.8
Address:	8.8.8.8#53

Non-authoritative answer:
Name:	ynov.com
Address: 172.67.74.226
Name:	ynov.com
Address: 104.26.11.233
Name:	ynov.com
Address: 104.26.10.233
```



lookup 78.74.21.21 :

```markdown 
mathias@MacBook-Air-de-Mathias ~ % nslookup 78.74.21.21
Server:		8.8.8.8
Address:	8.8.8.8#53

Non-authoritative answer:
21.21.74.78.in-addr.arpa	name = host-78-74-21-21.homerun.telia.com.

Authoritative answers can be found from:
```



lookup 92.146.54.88 :

```markdown 
mathias@MacBook-Air-de-Mathias ~ % nslookup 92.146.54.88
Server:		8.8.8.8
Address:	8.8.8.8#53

** server can't find 88.54.146.92.in-addr.arpa: NXDOMAIN
```
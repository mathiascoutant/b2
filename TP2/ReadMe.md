

les deux IPs choisies, en précisant le masque

```markdown 
mathias@MacBook-Air-de-Mathias ~ % ifconfig

en5: flags=8863<UP,BROADCAST,SMART,RUNNING,SIMPLEX,MULTICAST> mtu 1500
options=6467<RXCSUM,TXCSUM,VLAN_MTU,TSO4,TSO6,CHANNEL_IO,PARTIAL_CSUM,ZEROINVERT_CSUM>
ether 00:e0:4c:68:04:2c 
inet6 fe80::1082:3506:9869:1dbf%en5 prefixlen 64 secured scopeid 0x17 
inet 192.168.137.2 netmask 0xffffff00 broadcast 192.168.137.255
nd6 options=201<PERFORMNUD,DAD>
media: autoselect (1000baseT <full-duplex>)
status: active
```


```markdown 
PS C:\Users\heroeselfric> ipconfig /all
[...]

Carte Ethernet Ethernet :

Suffixe DNS propre à la connexion. . . :
Description. . . . . . . . . . . . . . : Killer E2600 Gigabit Ethernet Controller
Adresse physique . . . . . . . . . . . : 08-8F-C3-09-C8-CB
DHCP activé. . . . . . . . . . . . . . : Non
Configuration automatique activée. . . : Oui
Adresse IPv6 de liaison locale. . . . .: fe80::a898:99e2:5b95:5642%4(préféré)
Adresse IPv4. . . . . . . . . . . . . .: 192.168.88.129(préféré)
Masque de sous-réseau. . . . . . . . . : 255.255.255.192
Passerelle par défaut. . . . . . . . . : 192.168.88.130
IAID DHCPv6 . . . . . . . . . . . : 185110467
DUID de client DHCPv6. . . . . . . . : 00-01-00-01-28-85-64-89-08-8F-C3-09-C8-CB
Serveurs DNS. . .  . . . . . . . . . . : 8.8.8.8
[...]
```




l'adresse de réseau

```markdown 
0000 0000 . 0000 0000 . 0000 0000 . 1000 0000 réseau binaire
192.168.88.128/26
```



l'adresse de broadcast


```markdown 
0000 0000 . 0000 0000 . 0000 0000 . 1011 1111 bcast binaire
192.168.88.191/26
```





connexion est fonctionnelle entre les deux machines:

```markdown
mathias@MacBook-Air-de-Mathias ~ % ping 192.168.88.129
PING 192.168.88.129 (192.168.88.129): 56 data bytes
64 bytes from 192.168.88.129: icmp_seq=0 ttl=128 time=0.894 ms
64 bytes from 192.168.88.129: icmp_seq=1 ttl=128 time=0.779 ms
64 bytes from 192.168.88.129: icmp_seq=2 ttl=128 time=1.298 ms
^C
--- 192.168.88.129 ping statistics ---
3 packets transmitted, 3 packets received, 0.0% packet loss
round-trip min/avg/max/stddev = 0.779/0.990/1.298/0.223 ms
```


Wireshark it:
voir icmp.pcapng






la MAC de votre binome depuis votre table ARP :


```markdown
mathias@MacBook-Air-de-Mathias ~ % arp -a                    
(192.168.88.129) at 8:8f:c3:9:c8:cb on en5 ifscope [ethernet]
```

la MAC de la gateway de votre réseau:

```markdown

```


utilisez une commande pour vider votre table ARP:


```markdown
mathias@MacBook-Air-de-Mathias ~ % sudo arp -d -a
10.33.16.4 (10.33.16.4) deleted
10.33.16.8 (10.33.16.8) deleted
10.33.16.40 (10.33.16.40) deleted
10.33.16.43 (10.33.16.43) deleted
10.33.16.129 (10.33.16.129) deleted
10.33.16.135 (10.33.16.135) deleted
10.33.16.156 (10.33.16.156) deleted
10.33.16.158 (10.33.16.158) deleted
10.33.16.163 (10.33.16.163) deleted
10.33.16.171 (10.33.16.171) deleted
10.33.16.176 (10.33.16.176) deleted
10.33.16.190 (10.33.16.190) deleted
10.33.16.191 (10.33.16.191) deleted
10.33.16.193 (10.33.16.193) deleted
10.33.16.210 (10.33.16.210) deleted
10.33.16.214 (10.33.16.214) deleted
10.33.16.217 (10.33.16.217) deleted
10.33.16.224 (10.33.16.224) deleted
10.33.16.225 (10.33.16.225) deleted
10.33.16.235 (10.33.16.235) deleted
10.33.16.238 (10.33.16.238) deleted
10.33.16.239 (10.33.16.239) deleted
10.33.16.240 (10.33.16.240) deleted
10.33.16.246 (10.33.16.246) deleted
10.33.16.248 (10.33.16.248) deleted
10.33.16.251 (10.33.16.251) deleted
10.33.16.253 (10.33.16.253) deleted
10.33.17.0 (10.33.17.0) deleted
10.33.17.16 (10.33.17.16) deleted
10.33.17.55 (10.33.17.55) deleted
10.33.18.81 (10.33.18.81) deleted
10.33.18.82 (10.33.18.82) deleted
10.33.18.88 (10.33.18.88) deleted
10.33.18.93 (10.33.18.93) deleted
10.33.18.94 (10.33.18.94) deleted
10.33.18.95 (10.33.18.95) deleted
10.33.18.101 (10.33.18.101) deleted
10.33.18.106 (10.33.18.106) deleted
10.33.18.108 (10.33.18.108) deleted
10.33.18.109 (10.33.18.109) deleted
10.33.18.118 (10.33.18.118) deleted
10.33.18.121 (10.33.18.121) deleted
10.33.18.132 (10.33.18.132) deleted
10.33.18.133 (10.33.18.133) deleted
10.33.18.161 (10.33.18.161) deleted
10.33.18.184 (10.33.18.184) deleted
10.33.18.187 (10.33.18.187) deleted
10.33.18.194 (10.33.18.194) deleted
10.33.19.55 (10.33.19.55) deleted
10.33.19.56 (10.33.19.56) deleted
10.33.19.58 (10.33.19.58) deleted
10.33.19.62 (10.33.19.62) deleted
10.33.19.63 (10.33.19.63) deleted
10.33.19.65 (10.33.19.65) deleted
10.33.19.67 (10.33.19.67) deleted
10.33.19.68 (10.33.19.68) deleted
10.33.19.70 (10.33.19.70) deleted
10.33.19.71 (10.33.19.71) deleted
10.33.19.72 (10.33.19.72) deleted
10.33.19.74 (10.33.19.74) deleted
10.33.19.126 (10.33.19.126) deleted
10.33.19.134 (10.33.19.134) deleted
10.33.19.153 (10.33.19.153) deleted
10.33.19.159 (10.33.19.159) deleted
10.33.19.197 (10.33.19.197) deleted
10.33.19.209 (10.33.19.209) deleted
10.33.19.210 (10.33.19.210) deleted
10.33.19.225 (10.33.19.225) deleted
10.33.19.226 (10.33.19.226) deleted
10.33.19.227 (10.33.19.227) deleted
10.33.19.229 (10.33.19.229) deleted
10.33.19.232 (10.33.19.232) deleted
10.33.19.233 (10.33.19.233) deleted
10.33.19.234 (10.33.19.234) deleted
10.33.19.236 (10.33.19.236) deleted
10.33.19.238 (10.33.19.238) deleted
10.33.19.240 (10.33.19.240) deleted
10.33.19.241 (10.33.19.241) deleted
10.33.19.244 (10.33.19.244) deleted
10.33.19.245 (10.33.19.245) deleted
10.33.19.246 (10.33.19.246) deleted
10.33.19.248 (10.33.19.248) deleted
10.33.19.254 (10.33.19.254) deleted
169.254.183.73 (169.254.183.73) deleted
169.254.239.147 (169.254.239.147) deleted
192.168.88.128 (192.168.88.128) deleted
192.168.88.129 (192.168.88.129) deleted
224.0.0.251 (224.0.0.251) deleted
```

verifier que ca marche a nouveau:

```markdown
mathias@MacBook-Air-de-Mathias ~ % arp -a                    
? (10.33.16.158) at 82:98:9e:de:b5:41 on en0 ifscope [ethernet]
? (10.33.16.191) at a:f7:32:e1:8b:26 on en0 ifscope [ethernet]
? (10.33.16.195) at 8c:8d:28:35:4c:56 on en0 ifscope [ethernet]
? (10.33.16.196) at ec:2e:98:c0:e6:c9 on en0 ifscope [ethernet]
? (10.33.16.207) at a6:2f:e6:12:76:67 on en0 ifscope [ethernet]
? (10.33.16.210) at 2:77:e2:c7:65:da on en0 ifscope [ethernet]
? (10.33.16.214) at 1c:57:dc:3e:e4:16 on en0 ifscope [ethernet]
? (10.33.16.217) at a2:88:a7:67:f9:a8 on en0 ifscope [ethernet]
? (10.33.16.224) at 96:1d:a:51:d3:c on en0 ifscope [ethernet]
? (10.33.16.225) at 22:51:3f:bb:84:58 on en0 ifscope [ethernet]
? (10.33.16.232) at 7a:4d:5c:b3:ba:dd on en0 ifscope [ethernet]
? (10.33.16.235) at ae:70:8e:42:0:ad on en0 ifscope [ethernet]
? (10.33.16.238) at ec:ce:d7:f3:1b:3d on en0 ifscope [ethernet]
? (10.33.16.239) at ea:1d:1:29:3b:1 on en0 ifscope [ethernet]
? (10.33.16.240) at ba:b8:5c:a0:71:24 on en0 ifscope [ethernet]
? (10.33.16.242) at 3c:a6:f6:1e:22:c6 on en0 ifscope [ethernet]
? (10.33.16.243) at 32:86:76:6c:da:9d on en0 ifscope [ethernet]
? (10.33.16.246) at 6:c7:ef:64:51:bf on en0 ifscope [ethernet]
? (10.33.16.248) at 7a:f2:47:1c:23:78 on en0 ifscope [ethernet]
? (10.33.16.253) at 7e:a3:5f:6:e9:6 on en0 ifscope [ethernet]
? (10.33.17.0) at f8:4d:89:87:eb:ad on en0 ifscope [ethernet]
? (10.33.17.29) at 48:e7:da:56:17:17 on en0 ifscope [ethernet]
? (10.33.18.81) at 70:bb:e9:e6:3f:ed on en0 ifscope [ethernet]
? (10.33.18.83) at 48:5f:99:3:26:23 on en0 ifscope [ethernet]
? (10.33.18.93) at 4e:df:32:7c:a2:47 on en0 ifscope [ethernet]
? (10.33.18.95) at a2:e4:63:1d:2:93 on en0 ifscope [ethernet]
? (10.33.18.99) at 3c:a6:f6:2c:bf:5b on en0 ifscope [ethernet]
? (10.33.18.101) at 38:f9:d3:a7:6b:e9 on en0 ifscope [ethernet]
? (10.33.18.103) at 48:a4:72:b7:37:7f on en0 ifscope [ethernet]
? (10.33.18.108) at 26:90:3f:e2:cd:d2 on en0 ifscope [ethernet]
? (10.33.18.118) at 96:5c:7d:f5:30:9b on en0 ifscope [ethernet]
? (10.33.18.121) at fa:e7:6c:b:20:e3 on en0 ifscope [ethernet]
? (10.33.18.132) at f6:7a:23:89:cc:8e on en0 ifscope [ethernet]
? (10.33.18.136) at d8:f3:bc:7c:46:1f on en0 ifscope [ethernet]
? (10.33.18.184) at f0:18:98:77:9c:32 on en0 ifscope [ethernet]
? (10.33.18.194) at 78:4f:43:87:f5:11 on en0 ifscope [ethernet]
? (10.33.19.56) at d0:88:c:a1:83:34 on en0 ifscope [ethernet]
? (10.33.19.62) at 3c:6:30:3b:b5:3c on en0 ifscope [ethernet]
? (10.33.19.65) at 40:5b:d8:b7:d2:dd on en0 ifscope [ethernet]
? (10.33.19.67) at 14:7d:da:b8:23:1d on en0 ifscope [ethernet]
? (10.33.19.72) at 32:e3:cd:b6:d6:21 on en0 ifscope [ethernet]
? (10.33.19.74) at 8a:2e:ed:60:5a:4a on en0 ifscope [ethernet]
? (10.33.19.126) at 56:f5:b2:ca:92:9a on en0 ifscope [ethernet]
? (10.33.19.157) at 2:a4:ce:3e:c9:db on en0 ifscope [ethernet]
? (10.33.19.159) at 50:8e:49:5f:3b:fe on en0 ifscope [ethernet]
? (10.33.19.197) at ee:98:75:64:98:69 on en0 ifscope [ethernet]
? (10.33.19.226) at f6:a4:63:c8:7d:71 on en0 ifscope [ethernet]
? (10.33.19.230) at a4:c3:f0:f1:f1:44 on en0 ifscope [ethernet]
? (10.33.19.232) at 3c:6:30:20:e4:20 on en0 ifscope [ethernet]
? (10.33.19.236) at 3c:6:30:2f:a:7e on en0 ifscope [ethernet]
? (10.33.19.241) at c4:91:c:b3:aa:63 on en0 ifscope [ethernet]
? (10.33.19.246) at ee:4d:f2:e5:e7:a4 on en0 ifscope [ethernet]
? (10.33.19.247) at 18:56:80:ee:c4:fb on en0 ifscope [ethernet]
? (10.33.19.254) at 0:c0:e7:e0:4:4e on en0 ifscope [ethernet]
? (192.168.88.129) at 8:8f:c3:9:c8:cb on en5 ifscope [ethernet]
? (224.0.0.251) at 1:0:5e:0:0:fb on en0 ifscope permanent [ethernet]
```


on reping :

```markdown
mathias@MacBook-Air-de-Mathias ~ % ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8): 56 data bytes
64 bytes from 8.8.8.8: icmp_seq=0 ttl=114 time=23.475 ms
64 bytes from 8.8.8.8: icmp_seq=1 ttl=114 time=30.394 ms
^C
--- 8.8.8.8 ping statistics ---
2 packets transmitted, 2 packets received, 0.0% packet loss
round-trip min/avg/max/stddev = 23.475/26.934/30.394/3.459 ms
```

Wireshark it:

voir arp.pcapng


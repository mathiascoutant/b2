0 Préparation de la machine :

node1.tp1.b2 :


```

mathias@MacBook-Air-de-Mathias % sudo hostname node1.tp1.b2

mathias@MacBook-Air-de-Mathias % sudo echo "node1.tp1.b2" | sudo tee /etc/hostname node1.tp1.b2

```

node2.tp1.b2 :


```

mathias@MacBook-Air-de-Mathias % sudo hostname node2.tp1.b2

mathias@MacBook-Air-de-Mathias % sudo echo "node2.tp1.b2" | sudo tee /etc/hostname node2.tp1.b2

```


Acces a internet

node1.tp1.b2 :


```
[mathias@node1 ~]$ ping -c 4 8.8.8.8
    PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
    64 bytes from 8.8.8.8: icmp_seq=1 ttl=113 time=24.1 ms
    64 bytes from 8.8.8.8: icmp_seq=2 ttl=113 time=23.5 ms
    64 bytes from 8.8.8.8: icmp_seq=3 ttl=113 time=24.7 ms
    64 bytes from 8.8.8.8: icmp_seq=4 ttl=113 time=21.4 ms

    --- 8.8.8.8 ping statistics ---
    4 packets transmitted, 4 received, 0% packet loss, time 3006ms


```


node2.tp1.b2 :


```
[mathias@node2~]$ ping -c 4 8.8.8.8
    PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
    64 bytes from 8.8.8.8: icmp_seq=1 ttl=113 time=24.8 ms
    64 bytes from 8.8.8.8: icmp_seq=2 ttl=113 time=24.2 ms
    64 bytes from 8.8.8.8: icmp_seq=3 ttl=113 time=25.3 ms
    64 bytes from 8.8.8.8: icmp_seq=4 ttl=113 time=23.6 ms

    --- 8.8.8.8 ping statistics ---
    4 packets transmitted, 4 received, 0% packet loss, time 3005ms

```

I. Utilisateurs

1. Création et configuration

Sur la machine node1.tp1.b2 :

Créer utilisateur : 

```
[mathias@node1 ~]$ sudo useradd new -m -s /bin/bash
```

user exist ?

```
[mathais@node1 ~]$ sudo cat /etc/passwd | grep "new"
new:x:1001:1001::/home/new:/bin/bash
```



Sur la machine node2.tp1.b2 :

Créer utilisateur :


```
[mathias@node2 ~]$ sudo useradd new -m -s /bin/bash
```


user exist ?

```
[mathias@node2 ~]$ sudo cat /etc/passwd | grep "new"
new:x:1001:1001::/home/new:/bin/bash
```



Créer un nouveau groupe admins

node1.tp1.b2 :

new group admin :

```
[mathias@node1 ~]$ sudo groupadd admins
```

ajout de perm: 

```
[mathias@node1 ~]$ sudo visudo /etc/sudoers
[mathias@node1 ~]$ sudo cat /etc/sudoers | grep %admins
%admins ALL=(ALL)       ALL

```

node2.tp1.b2 :

new group admin :

```
[mathias@node2 ~]$ sudo groupadd admins
```

ajout de perm: 

```
[mathias@node2 ~]$ sudo visudo /etc/sudoers
[mathias@node2 ~]$ sudo cat /etc/sudoers | grep %admins
%admins ALL=(ALL)       ALL

```


ajouter user au groupe admin :


node1.tp1.b2 :

```
[mathias@node1 ~]$ sudo usermod -aG admins new
[mathias@node1 ~]$ groups new
new : new admins
```


node2.tp1.b2 :

```
[mathias@node2 ~]$ sudo usermod -aG admins new
[mathias@node2 ~]$ groups new
new : new admins
```

2. SSH


échange de clés SSH :

```
mathias@MacBook-Air-de-Mathias ~ % ssh-keygen -t rsa -b 4096
Generating public/private rsa key pair.
Enter file in which to save the key (/Users/mathias/.ssh/id_rsa): 
/Users/mathias/.ssh/id_rsa already exists.
Overwrite (y/n)? y
Enter passphrase (empty for no passphrase): 
Enter same passphrase again: 
Your identification has been saved in /Users/mathias/.ssh/id_rsa
Your public key has been saved in /Users/mathias/.ssh/id_rsa.pub
The key fingerprint is:
SHA256:Bx5RhHSH8ckdZKGitzIGjcTklJgS1DzwNi6ScS2ga5E mathias@MacBook-Air-de-Mathias.local
The key's randomart image is:
+---[RSA 4096]----+
|..+= oooo+=o..=. |
|..oo*=. .o.+ = . |
|oEo.=.+ o . = .  |
| =.+ o + + .     |
|+.. . o S o      |
|.. .   . o .     |
|        + .      |
|       . o       |
|                 |
+----[SHA256]-----+
```


Verifier que la connexion SSH fonctionne :

node1.tp1.b2 :

mathias@MacBook-Air-de-Mathias ~ % ssh new@10.101.1.11
Last login: Thu Nov 24 19:15:25 2022 from 10.101.1.1
[new@node1 ~]$



node2.tp1.b2 :

mathias@MacBook-Air-de-Mathias ~ % ssh new@10.101.1.12
Last login: Thu Nov 24 19:15:25 2022
[new@node1 ~]$
